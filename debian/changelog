libtest-nobreakpoints-perl (0.17-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtest-nobreakpoints-perl: Add Multi-Arch:
    foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 19:05:39 +0100

libtest-nobreakpoints-perl (0.17-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtest-simple-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 28 Aug 2022 15:47:56 +0100

libtest-nobreakpoints-perl (0.17-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ gregor herrmann ]
  * Import upstream version 0.17.
  * Update debian/upstream/metadata.
  * Update years of packaging copyright.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Feb 2021 01:52:54 +0100

libtest-nobreakpoints-perl (0.15-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::Tester build dependency.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Add (empty) debian/tests/pkg-perl/smoke-skip to fix regression from
    new pkg-perl-tools-autopkgtest; it removes files in t/ which are
    needed by other tests.
  * Update years of packaging copyright.
  * Bump debhelper compatibility level to 9.
  * Update alternative build dependency.
  * Declare compliance with Debian Policy 4.0.1.

 -- gregor herrmann <gregoa@debian.org>  Tue, 15 Aug 2017 18:56:15 +0200

libtest-nobreakpoints-perl (0.15-1) unstable; urgency=low

  * New upstream release.
  * debian/rules: remove override_dh_auto_test (AUTOMATED_TESTING is
    gone).

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Feb 2012 14:42:14 +0100

libtest-nobreakpoints-perl (0.14-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Switch to "3.0 (quilt)" source format. Remove quilt framework.
  * Remove patch, applied upstream.
  * debian/copyright: add new copyright holder, update years of upstream
    and packaging copyright, refresh license stanzas.
  * Switch to debhelper compatibility level 8.
  * Bump Standards-Version to 3.9.2 (no changes).
  * Remove build dependency on libtest-warn-perl. Add build dependency on
    libtest-useallmodules-perl.
  * Add /me to Uploaders.
  * debian/rules: pass AUTOMATED_TESTING to test suite.

 -- gregor herrmann <gregoa@debian.org>  Fri, 13 Jan 2012 16:36:33 +0100

libtest-nobreakpoints-perl (0.13-1) unstable; urgency=low

  * Initial Release (Closes: #564775)

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 15 Jan 2010 20:20:24 -0500
